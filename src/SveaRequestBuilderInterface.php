<?php

namespace Drupal\commerce_svea;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_price\Price;

/**
 * Provides the Svea request builder interface.
 */
interface SveaRequestBuilderInterface {

  /**
   * Builds the order array for the given order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return array
   *   The order request array.
   */
  public function buildOrder(OrderInterface $order): array;

  /**
   * Builds the order rows for Svea.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return array
   *   The order rows as expected by Svea.
   */
  public function buildOrderRows(OrderInterface $order): array;

  /**
   * Converts the given amount to its minor units.
   *
   * For example, 9.99 USD becomes 999 (Copied from PaymentGatewayBase).
   *
   * @param \Drupal\commerce_price\Price $amount
   *   The amount.
   *
   * @return int
   *   The amount in minor units, as an integer.
   */
  public function toMinorUnits(Price $amount): int;

}
