<?php

namespace Drupal\commerce_svea;

use CommerceGuys\Addressing\Country\CountryRepositoryInterface;
use Drupal\commerce_order\AdjustmentTransformerInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_price\Calculator;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Prepares request before sending it to Svea.
 */
class SveaRequestBuilder implements SveaRequestBuilderInterface {

  use StringTranslationTrait;

  /**
   * The adjustment transformer.
   *
   * @var \Drupal\commerce_order\AdjustmentTransformerInterface
   */
  protected $adjustmentTransformer;

  /**
   * The country repository.
   *
   * @var \CommerceGuys\Addressing\Country\CountryRepositoryInterface
   */
  protected $countryRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new SveaRequestBuilder object.
   *
   * @param \Drupal\commerce_order\AdjustmentTransformerInterface $adjustment_transformer
   *   The adjustment transformer.
   * @param \CommerceGuys\Addressing\Country\CountryRepositoryInterface $country_repository
   *   The country repository.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(AdjustmentTransformerInterface $adjustment_transformer, CountryRepositoryInterface $country_repository, EntityTypeManagerInterface $entity_type_manager) {
    $this->adjustmentTransformer = $adjustment_transformer;
    $this->countryRepository = $country_repository;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOrder(OrderInterface $order): array {
    $data = [];
    $data['Cart']['Items'] = $this->buildOrderRows($order);
    $data['Currency'] = $order->getTotalPrice()->getCurrencyCode();
    $data['ClientOrderNumber'] = $order->id();
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOrderRows(OrderInterface $order): array {
    $order_rows = [];
    foreach ($order->getItems() as $order_item) {
      // Fallback to the order item ID.
      $article_number = $order_item->id();
      $purchased_entity = $order_item->getPurchasedEntity();

      // Send the "SKU" as the "ArticleNumber" for product variations.
      if ($purchased_entity instanceof ProductVariationInterface) {
        $article_number = $purchased_entity->getSku();
      }

      $order_row = [
        'ArticleNumber' => $article_number,
        // Name is limited to 40 characters.
        'Name' => mb_substr($order_item->label(), 0, 40),
        // Use minor value.
        'Quantity' => (int) Calculator::multiply($order_item->getQuantity(), 100),
        'UnitPrice' => $this->toMinorUnits($order_item->getUnitPrice()),
        'VatPercent' => 0,
        'Unit' => 'st',
      ];

      // Only pass included tax adjustments (i.e "VAT"), non included tax
      // adjustments are passed separately (i.e "Sales tax").
      $tax_adjustments = $order_item->getAdjustments(['tax']);
      if ($tax_adjustments && $tax_adjustments[0]->isIncluded()) {
        $tax_rate = $tax_adjustments[0]->getPercentage();
        $order_row = array_merge($order_row, [
          // Use minor value.
          'VatPercent' => (int) Calculator::multiply($tax_rate, '10000'),
        ]);
      }

      // Check if we have promotion adjustments.
      $promotion_adjustments = $order_item->getAdjustments(['promotion']);
      if ($promotion_adjustments) {
        // Use minor value.
        $order_row['DiscountAmount'] = Calculator::multiply($this->getAdjustmentsTotal($promotion_adjustments, [], TRUE), '10000');
      }

      $order_rows[] = $order_row;
    }

    // Shipping is handled separately.
    $exclude_adjustment_types = ['shipping', 'promotion', 'shipping_promotion'];
    $adjustments = $order->collectAdjustments();
    $adjustments = $adjustments ? $this->adjustmentTransformer->processAdjustments($adjustments) : [];
    foreach ($adjustments as $adjustment) {
      $adjustment_type = $adjustment->getType();
      // Skip included adjustments and the ones we don't handle.
      if ($adjustment->isIncluded() || in_array($adjustment_type, $exclude_adjustment_types)) {
        continue;
      }
      $order_row = [
        'ArticleNumber' => $adjustment->getSourceId(),
        // Name is limited to 40 characters.
        'Name' => mb_substr($adjustment->getLabel(), 0, 40),
        // Use minor value for quantity.
        'Quantity' => 100,
        'VatPercent' => 0,
        'UnitPrice' => $this->toMinorUnits($adjustment->getAmount()),
      ];

      $order_rows[] = $order_row;
    }

    if ($order->hasField('shipments') && !$order->get('shipments')->isEmpty()) {
      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface[] $shipments */
      $shipments = $order->get('shipments')->referencedEntities();
      foreach ($shipments as $shipment) {
        $shipment_amount = $shipment->getAmount();
        if (!$shipment_amount) {
          continue;
        }

        // Check if there are included tax adjustments.
        $tax_adjustments = array_filter($shipment->getAdjustments(['tax']), function ($adjustment) {
          return $adjustment->isIncluded();
        });

        $shipping_line = [
          'Name' => (string) $this->t('Shipping'),
          'Quantity' => 100,
          'RowType' => 'ShippingFee',
        ];
        // If there are no included tax adjustments, there's no need to split
        // the shipment amounts across multiple VAT rates.
        if (!$tax_adjustments) {
          $shipping_line += [
            'VatPercent' => 0,
          ];
          // The unit price shouldn't include discounts, we cannot use the
          // original amount as the "unitPrice" because it won't reflect the
          // correct price if shipping rates were altered.
          $unit_price = $shipment->getAmount();
          $promotion_adjustments = $shipment->getAdjustments(['shipping_promotion']);
          if ($promotion_adjustments && ($promotions_total = $this->getAdjustmentsTotal($promotion_adjustments, [], TRUE))) {
            $shipping_line['DiscountAmount'] = $this->toMinorUnits($promotions_total->multiply('-1'));
          }
          $shipping_line['UnitPrice'] = $this->toMinorUnits($unit_price);

          $order_rows[] = $shipping_line;
        }
        else {
          /** @var \Drupal\commerce_svea\SveaShipmentPriceSplitter $shipment_price_splitter */
          $shipment_price_splitter = \Drupal::service('commerce_svea.shipment_price_splitter');
          $shipment_amounts = $shipment_price_splitter->split($order, $shipment);
          foreach ($shipment_amounts as $amounts) {
            $order_rows[] = $shipping_line + [
              'UnitPrice' => $this->toMinorUnits($amounts['unit_amount']),
              'VatPercent' => (int) Calculator::multiply($amounts['tax_rate'], '10000'),
              'DiscountAmount' => $this->toMinorUnits($amounts['promotions_amount']),
            ];
          }
        }
      }
    }

    return $order_rows;
  }

  /**
   * Calculates the total for the given adjustments.
   *
   * @param \Drupal\commerce_order\Adjustment[] $adjustments
   *   The adjustments.
   * @param string[] $adjustment_types
   *   The adjustment types to include in the calculation.
   *   Examples: fee, promotion, tax. Defaults to all adjustment types.
   * @param bool $skip_included
   *   Whether to skip included adjustments (Defaults to FALSE).
   *
   * @return \Drupal\commerce_price\Price|null
   *   The adjustments total, or NULL if no matching adjustments were found.
   */
  protected function getAdjustmentsTotal(array $adjustments, array $adjustment_types = [], $skip_included = FALSE) {
    $adjustments_total = NULL;
    $matching_adjustments = [];

    foreach ($adjustments as $adjustment) {
      if ($skip_included && $adjustment->isIncluded()) {
        continue;
      }
      if ($adjustment_types && !in_array($adjustment->getType(), $adjustment_types)) {
        continue;
      }
      $matching_adjustments[] = $adjustment;
    }
    if ($matching_adjustments) {
      $matching_adjustments = $this->adjustmentTransformer->processAdjustments($matching_adjustments);
      foreach ($matching_adjustments as $adjustment) {
        $adjustments_total = $adjustments_total ? $adjustments_total->add($adjustment->getAmount()) : $adjustment->getAmount();
      }
    }

    return $adjustments_total;
  }

  /**
   * Converts the given amount to its minor units.
   *
   * For example, 9.99 USD becomes 999 (Copied from PaymentGatewayBase).
   *
   * @param \Drupal\commerce_price\Price $amount
   *   The amount.
   *
   * @return int
   *   The amount in minor units, as an integer.
   */
  public function toMinorUnits(Price $amount): int {
    $currency_storage = $this->entityTypeManager->getStorage('commerce_currency');
    /** @var \Drupal\commerce_price\Entity\CurrencyInterface $currency */
    $currency = $currency_storage->load($amount->getCurrencyCode());
    $fraction_digits = $currency->getFractionDigits();
    $number = $amount->getNumber();
    if ($fraction_digits > 0) {
      $number = Calculator::multiply($number, pow(10, $fraction_digits));
    }

    return (int) round($number);
  }

}
