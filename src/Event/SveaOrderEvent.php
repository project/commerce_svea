<?php

namespace Drupal\commerce_svea\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Represents a Svea order event.
 *
 * @see \Drupal\commerce_svea\Event\SveaCheckoutEvents
 */
class SveaOrderEvent extends Event {

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The Svea order.
   *
   * @var array
   */
  protected $sveaOrder;

  /**
   * Constructs a new SveaOrderEvent object.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param array $svea_order
   *   The Svea order.
   */
  public function __construct(OrderInterface $order, array $svea_order = []) {
    $this->order = $order;
    $this->sveaOrder = $svea_order;
  }

  /**
   * Gets the commerce order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The commerce order entity.
   */
  public function getOrder(): OrderInterface {
    return $this->order;
  }

  /**
   * Gets the Svea order.
   *
   * @return array
   *   The Svea order.
   */
  public function getSveaOrder(): array {
    return $this->sveaOrder;
  }

}
