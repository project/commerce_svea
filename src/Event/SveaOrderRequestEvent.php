<?php

namespace Drupal\commerce_svea\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Defines the order request event.
 *
 * @see \Drupal\commerce_svea\Event\SveaCheckoutEvents
 */
class SveaOrderRequestEvent extends Event {

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The API request data.
   *
   * @var array
   */
  protected $requestData;

  /**
   * Constructs a new SveaOrderRequestEvent object.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param array $request_data
   *   The API request data.
   */
  public function __construct(OrderInterface $order, array $request_data) {
    $this->order = $order;
    $this->requestData = $request_data;
  }

  /**
   * Gets the order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order.
   */
  public function getOrder(): OrderInterface {
    return $this->order;
  }

  /**
   * Gets the API request data.
   *
   * @return array
   *   The API request data.
   */
  public function getRequestData(): array {
    return $this->requestData;
  }

}
