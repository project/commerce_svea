<?php

namespace Drupal\commerce_svea\Event;

/**
 * Defines events for the Commerce Svea module.
 */
final class SveaCheckoutEvents {

  /**
   * Name of the event fired when calling Svea for creating an order.
   *
   * @Event
   *
   * @see \Drupal\commerce_svea\Event\SveaOrderRequestEvent
   */
  const CREATE_ORDER_REQUEST = 'commerce_svea.create_order_request';

  /**
   * Name of the event fired when calling Svea for updating an order.
   *
   * @Event
   *
   * @see \Drupal\commerce_svea\Event\SveaOrderRequestEvent
   */
  const UPDATE_ORDER_REQUEST = 'commerce_svea.update_order_request';

  /**
   * Name of the event fired when Svea sending callback validation request.
   *
   * @Event
   *
   * @see \Drupal\commerce_svea\Event\SveaOrderEvent
   */
  const ORDER_VALIDATION = 'commerce_svea.order_validation';

  /**
   * Name of the event fired when Svea order is acknowledged.
   *
   * @Event
   *
   * @see \Drupal\commerce_svea\Event\SveaOrderEvent
   */
  const ACKNOWLEDGE_ORDER = 'commerce_svea.acknowledge_order';

}
