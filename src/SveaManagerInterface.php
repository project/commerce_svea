<?php

namespace Drupal\commerce_svea;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Provides the Svea manager interface.
 */
interface SveaManagerInterface {

  /**
   * Gets the Svea order for the given commerce order.
   *
   * @param int $svea_order_id
   *   The Svea order id.
   *
   * @return array
   *   The Svea order data array.
   */
  public function getOrder(int $svea_order_id): array;

  /**
   * Creates a new Svea order for the given commerce order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The commerce order.
   * @param array $merchant_settings
   *   An associative array containing at least the following keys:
   *   - 'TermsUri': The url to the terms and conditions page.
   *   - 'CheckoutUri': The url to the checkout page.
   *   - 'ConfirmationUri': The url of the checkout confirmation page.
   *   - 'PushUri': URL that will be requested when an order is completed.
   *
   * @return array
   *   The Svea order data array.
   *
   * @see https://checkoutapi.svea.com/docs/#/data-types?id=createordermodel
   */
  public function createOrder(OrderInterface $order, array $merchant_settings): array;

  /**
   * Updates the order in Svea.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param array $merchant_settings
   *   An associative array containing at least the following keys:
   *   - 'TermsUri': The url to the terms and conditions page.
   *   - 'CheckoutUri': The url to the checkout page.
   *   - 'ConfirmationUri': The url of the checkout confirmation page.
   *   - 'PushUri': URL that will be requested when an order is completed.
   *
   * @return array
   *   The Svea order data array.
   *
   * @see https://checkoutapi.svea.com/docs/#/data-types?id=updateordermodel
   */
  public function updateOrder(OrderInterface $order, array $merchant_settings): array;

  /**
   * Cancels the Svea order for the given commerce order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The commerce order.
   */
  public function cancelOrder(OrderInterface $order);

}
