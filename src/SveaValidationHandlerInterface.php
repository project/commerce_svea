<?php

namespace Drupal\commerce_svea;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides the Svea validation handler interface.
 */
interface SveaValidationHandlerInterface {

  /**
   * Processes the order validation request from Svea.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The order validation request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The validation callback response.
   */
  public function validateOrder(Request $request): Response;

}
