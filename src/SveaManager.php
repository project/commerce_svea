<?php

namespace Drupal\commerce_svea;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_svea\Event\SveaCheckoutEvents;
use Drupal\commerce_svea\Event\SveaOrderRequestEvent;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Svea\Checkout\CheckoutAdminClient;
use Svea\Checkout\CheckoutClient;
use Svea\Checkout\Transport\Connector;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Manages Svea related operations.
 */
class SveaManager implements SveaManagerInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The request builder.
   *
   * @var \Drupal\commerce_svea\SveaRequestBuilderInterface
   */
  protected $requestBuilder;

  /**
   * The payment gateway plugin configuration.
   *
   * @var array
   */
  protected $config;

  /**
   * The Svea checkout client.
   *
   * @var \Svea\Checkout\CheckoutClient
   */
  protected $checkoutClient;

  /**
   * The Svea checkout admin client.
   *
   * @var \Svea\Checkout\CheckoutAdminClient
   */
  protected $checkoutAdminClient;

  /**
   * Constructs a new SveaManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\commerce_svea\SveaRequestBuilderInterface $request_builder
   *   The request builder.
   * @param array $config
   *   The payment gateway plugin configuration array.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher, SveaRequestBuilderInterface $request_builder, array $config) {
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->requestBuilder = $request_builder;
    $this->config = $config;
    $this->initCheckoutClient();
  }

  /**
   * Initializes the Svea checkout client.
   */
  protected function initCheckoutClient() {
    $base_url = $this->config['mode'] === 'test' ? Connector::TEST_BASE_URL : Connector::PROD_BASE_URL;
    $connector = Connector::init(
      $this->config['merchant_id'],
      $this->config['merchant_secret'],
      $base_url
    );
    $this->checkoutClient = new CheckoutClient($connector);
  }

  /**
   * Initializes the Svea checkout admin client.
   */
  protected function initCheckoutAdminClient() {
    $base_url = $this->config['mode'] === 'test' ? Connector::TEST_ADMIN_BASE_URL : Connector::PROD_ADMIN_BASE_URL;
    $connector = Connector::init(
      $this->config['merchant_id'],
      $this->config['merchant_secret'],
      $base_url
    );
    $this->checkoutAdminClient = new CheckoutAdminClient($connector);
  }

  /**
   * {@inheritdoc}
   */
  public function getOrder(int $svea_order_id): array {
    return $this->checkoutClient->get(['orderId' => $svea_order_id]);
  }

  /**
   * {@inheritdoc}
   */
  public function createOrder(OrderInterface $order, array $merchant_settings): array {
    $request_data = $this->buildOrderRequest($order, $merchant_settings);
    $event = new SveaOrderRequestEvent($order, $request_data);
    $this->eventDispatcher->dispatch(SveaCheckoutEvents::CREATE_ORDER_REQUEST, $event);
    return $this->checkoutClient->create($event->getRequestData());
  }

  /**
   * {@inheritdoc}
   */
  public function updateOrder(OrderInterface $order, array $merchant_settings): array {
    $request_data = $this->buildOrderRequest($order, $merchant_settings);

    // Pass Svea order id here also.
    $request_data['OrderId'] = $order->getData('svea_order_id');

    $event = new SveaOrderRequestEvent($order, $request_data);
    $this->eventDispatcher->dispatch(SveaCheckoutEvents::UPDATE_ORDER_REQUEST, $event);
    return $this->checkoutClient->update($event->getRequestData());
  }

  /**
   * Builds the Svea order request array for the given commerce order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param array $merchant_settings
   *   The merchant settings (URLs).
   *
   * @return array
   *   The order request array.
   */
  protected function buildOrderRequest(OrderInterface $order, array $merchant_settings): array {
    if (is_null($order->getTotalPrice())) {
      throw new \InvalidArgumentException(sprintf('Cannot build the order request with an empty order total for order id %s.', $order->id()));
    }

    $terms_path = $this->config['terms_path'];
    $terms_uri = UrlHelper::isExternal($terms_path) ? Url::fromUri($terms_path) : Url::fromUserInput('/' . ltrim($terms_path, '/'), ['absolute' => TRUE]);
    $data = [
      'CountryCode' => $this->config['purchase_country'],
      'Locale' => $this->config['locale'],
      'MerchantSettings' => $merchant_settings + [
        'TermsUri' => $terms_uri->toString(),
      ],
    ];

    $data += $this->requestBuilder->buildOrder($order);

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function cancelOrder(OrderInterface $order) {
    $this->assertOrder($order);

    // Right for now we need check admin client for order canceling, so do not
    // pass it to the main constructor.
    $this->initCheckoutAdminClient();
    $this->checkoutAdminClient->cancelOrder(['orderId' => $order->getData('svea_order_id')]);
  }

  /**
   * Asserts that the order holds a reference to the Svea order ID.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @throws \InvalidArgumentException
   *   Thrown if the order doesn't hold a reference to the SVEA order ID.
   */
  protected function assertOrder(OrderInterface $order) {
    if (!$order->getData('svea_order_id')) {
      throw new \InvalidArgumentException(sprintf('Missing Svea order ID for commerce order %s.', $order->id()));
    }
  }

}
