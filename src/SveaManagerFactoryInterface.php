<?php

namespace Drupal\commerce_svea;

/**
 * Provides the Svea manager factory interface.
 */
interface SveaManagerFactoryInterface {

  /**
   * Instantiate a new Svea manager for the given config.
   *
   * @param array $configuration
   *   An associative array, containing at least the following keys:
   *   - mode: The API mode (e.g "test" or "live").
   *   - username: The API username.
   *   - password: The API password.
   *   - terms_path: The path to the terms and conditions page.
   *
   * @return \Drupal\commerce_svea\SveaManagerInterface
   *   The Svea manager.
   */
  public function get(array $configuration): SveaManagerInterface;

}
