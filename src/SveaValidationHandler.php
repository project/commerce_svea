<?php

namespace Drupal\commerce_svea;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_svea\Event\SveaCheckoutEvents;
use Drupal\commerce_svea\Event\SveaOrderEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Contains Svea validation methods.
 */
class SveaValidationHandler implements SveaValidationHandlerInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The key value factory.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected $keyValueFactory;

  /**
   * Constructs a new SveaValidationHandler object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value_factory
   *   The key value factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher, KeyValueFactoryInterface $key_value_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->keyValueFactory = $key_value_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function validateOrder(Request $request): Response {
    $order_id = $request->query->get('client_order_number');
    // @see https://checkoutapi.svea.com/docs/#/data-types?id=checkoutvalidationcallbackresponse.
    $data = [
      'Valid' => FALSE,
      'ClientOrderNumber' => '',
      'Message' => '',
    ];
    if (!$order_id) {
      $data['Message'] = 'Cannot find a client order number in the request.';
      return new JsonResponse($data, Response::HTTP_NOT_FOUND);
    }
    $data['ClientOrderNumber'] = $order_id;

    $order_storage = $this->entityTypeManager->getStorage('commerce_order');
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $order_storage->load($order_id);
    if (!$order) {
      $svea_order_id = $request->query->get('svea_order_id');
      $data['Message'] = sprintf('Cannot find a matching order for the given Svea order ID %s.', $svea_order_id);
      return new JsonResponse($data, Response::HTTP_NOT_FOUND);
    }

    try {
      $event = new SveaOrderEvent($order);
      $this->eventDispatcher->dispatch(SveaCheckoutEvents::ORDER_VALIDATION, $event);

      // Lock order and proceed to confirmation.
      if (!$order->isLocked()) {
        $order->lock();
        $order->setRefreshState(OrderInterface::REFRESH_SKIP);
        $order->save();
      }

      // Set order as valid.
      $data['Valid'] = TRUE;
      return new JsonResponse($data);
    }
    catch (\Exception $exception) {
      $key_value_store = $this->keyValueFactory->get('commerce_svea.order_validation');
      // Store in the key value a flag indicating the Svea order could not
      // be validated properly.
      $key_value_store->set('order:' . $order->id(), TRUE);

      // Set message from failed validation.
      $data['Message'] = $exception->getMessage();
      return new JsonResponse($data, Response::HTTP_UNPROCESSABLE_ENTITY);
    }
  }

}
