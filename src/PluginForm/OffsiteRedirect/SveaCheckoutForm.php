<?php

namespace Drupal\commerce_svea\PluginForm\OffsiteRedirect;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\commerce_svea\SveaManagerFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\Markup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Svea checkout form.
 */
class SveaCheckoutForm extends PaymentOffsiteForm implements ContainerInjectionInterface {

  /**
   * The SVEA manager factory.
   *
   * @var \Drupal\commerce_svea\SveaManagerFactoryInterface
   */
  protected $sveaManagerFactory;

  /**
   * The key-value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValueStore;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new SveaCheckoutForm object.
   *
   * @param \Drupal\commerce_svea\SveaManagerFactoryInterface $svea_manager_factory
   *   The Svea manager factory.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value_factory
   *   The key value factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(SveaManagerFactoryInterface $svea_manager_factory, KeyValueFactoryInterface $key_value_factory, MessengerInterface $messenger) {
    $this->sveaManagerFactory = $svea_manager_factory;
    $this->keyValueStore = $key_value_factory->get('commerce_svea.order_validation');
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_svea.manager_factory'),
      $container->get('keyvalue'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    $order = $payment->getOrder();
    /** @var \Drupal\commerce_svea\Plugin\Commerce\PaymentGateway\SveaCheckoutInterface $plugin */
    $plugin = $this->plugin;
    $configuration = $plugin->getConfiguration();
    $svea_manager = $this->sveaManagerFactory->get($configuration);

    // Check if we already have a Svea order ID for this order.
    $merchant_settings = [
      'CheckoutUri' => $form['#cancel_url'],
      'ConfirmationUri' => $form['#return_url'],
      // We need to decode the push uri because of {checkout.order.uri}.
      'PushUri' => urldecode($plugin->getNotifyUrl()->toString()),
    ];

    // Add validation callback if needed.
    if (!empty($configuration['enable_order_validation'])) {
      $validation_url = $plugin->getValidationUrl();
      $validation_url->mergeOptions([
        'query' => [
          'client_order_number' => $order->id(),
        ],
      ]);
      $merchant_settings['CheckoutValidationCallBackUri'] = urldecode($validation_url->toString());
    }

    if ($order->getData('svea_order_id')) {
      try {
        $svea_order = $svea_manager->updateOrder($order, $merchant_settings);
      }
      catch (\Exception $exception) {
        // The Svea order ID might be invalid, proceed to creating a new one.
        $svea_order = NULL;
      }
    }

    if (!isset($svea_order)) {
      try {
        $svea_order = $svea_manager->createOrder($order, $merchant_settings);
        $order->setData('svea_order_id', $svea_order['OrderId']);
        $order->setRefreshState(OrderInterface::REFRESH_SKIP);
        $order->save();
      }
      catch (\Exception $exception) {
        throw new PaymentGatewayException($exception->getMessage());
      }
    }

    // Display validation error if one set.
    if ($this->keyValueStore->get('order:' . $order->id())) {
      $this->messenger->addError($this->t('Payment failed at the payment server. Please review your information and try again.'));
      $this->keyValueStore->delete('order:' . $order->id());
    }

    $form['svea'] = [
      '#markup' => Markup::create($svea_order['Gui']['Snippet']),
    ];

    return $form;
  }

}
