<?php

namespace Drupal\commerce_svea;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Defines a factory for the Svea manager.
 */
class SveaManagerFactory implements SveaManagerFactoryInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The request builder.
   *
   * @var \Drupal\commerce_svea\SveaRequestBuilderInterface
   */
  protected $requestBuilder;

  /**
   * Array of all instantiated Svea managers.
   *
   * @var \Drupal\commerce_svea\SveaManagerInterface[]
   */
  protected $instances = [];

  /**
   * Constructs a new SveaManagerFactory object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\commerce_svea\SveaRequestBuilderInterface $request_builder
   *   The request builder.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher, SveaRequestBuilderInterface $request_builder) {
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->requestBuilder = $request_builder;
  }

  /**
   * {@inheritdoc}
   */
  public function get(array $configuration): SveaManagerInterface {
    $merchant = $configuration['merchant_id'];
    if (!isset($this->instances[$merchant])) {
      $this->instances[$merchant] = new SveaManager($this->entityTypeManager, $this->eventDispatcher, $this->requestBuilder, $configuration);
    }

    return $this->instances[$merchant];
  }

}
