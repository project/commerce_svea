<?php

namespace Drupal\commerce_svea\EventSubscriber;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_svea\Plugin\Commerce\PaymentGateway\SveaCheckoutInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Svea order subscriber.
 */
class SveaOrderSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'commerce_order.cancel.post_transition' => ['onCancel'],
    ];
  }

  /**
   * Cancels the Svea order when the order is canceled.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function onCancel(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    if (!$this->isPaidWithSvea($order)) {
      return;
    }
    /** @var \Drupal\commerce_svea\Plugin\Commerce\PaymentGateway\SveaCheckoutInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $order->get('payment_gateway')->entity->getPlugin();
    $payment_gateway_plugin->cancelOrder($order);
  }

  /**
   * Returns whether the order's payment gateway is Svea.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return bool
   *   TRUE, if the order's payment gateway is Svea.
   */
  protected function isPaidWithSvea(OrderInterface $order): bool {
    $payment_gateway = !$order->get('payment_gateway')->isEmpty() ? $order->get('payment_gateway')->entity : FALSE;
    return $payment_gateway instanceof PaymentGatewayInterface && $payment_gateway->getPlugin() instanceof SveaCheckoutInterface;
  }

}
