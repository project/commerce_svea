<?php

namespace Drupal\commerce_svea\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_svea\SveaManagerFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Svea checkout confirmation snippet.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_svea_checkout_confirmation",
 *   label = @Translation("Svea Confirmation message"),
 *   default_step = "complete",
 * )
 */
class SveaCheckoutConfirmation extends CheckoutPaneBase {

  /**
   * The Svea manager factory.
   *
   * @var \Drupal\commerce_svea\SveaManagerFactoryInterface
   */
  protected $sveaManagerFactory;

  /**
   * Constructs a new SveaCheckoutConfirmation object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface $checkout_flow
   *   The parent checkout flow.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_svea\SveaManagerFactoryInterface $svea_manager_factory
   *   The Svea manager factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager, SveaManagerFactoryInterface $svea_manager_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);
    $this->sveaManagerFactory = $svea_manager_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow,
      $container->get('entity_type.manager'),
      $container->get('commerce_svea.manager_factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form): array {
    try {
      /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
      $payment_gateway = $this->order->payment_gateway->entity;
      /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
      $payment_gateway_plugin = $payment_gateway->getPlugin();
      $svea_manager = $this->sveaManagerFactory->get($payment_gateway_plugin->getConfiguration());
      $svea_order = $svea_manager->getOrder($this->order->getData('svea_order_id'));
      $pane_form['svea_completion'] = [
        '#markup' => Markup::create($svea_order['Gui']['Snippet']),
      ];
      return $pane_form;
    }
    catch (\Exception $exception) {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible(): bool {
    if (!$this->order->hasField('payment_gateway') || $this->order->get('payment_gateway')->isEmpty()) {
      return FALSE;
    }

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $this->order->get('payment_gateway')->entity;
    return $payment_gateway && $payment_gateway->getPluginId() == 'svea_checkout';
  }

}
