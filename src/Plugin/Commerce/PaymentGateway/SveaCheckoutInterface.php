<?php

namespace Drupal\commerce_svea\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Url;

/**
 * Provides the interface for the Svea Checkout payment gateway.
 */
interface SveaCheckoutInterface extends OffsitePaymentGatewayInterface, SupportsAuthorizationsInterface {

  /**
   * Gets the URL to the "validation" page.
   *
   * When supported, this page is called asynchronously to validate payment
   * before submitting at Svea.
   *
   * @return \Drupal\Core\Url
   *   The "validation" page url.
   */
  public function getValidationUrl(): Url;

  /**
   * Cancels the Svea order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order to cancel.
   */
  public function cancelOrder(OrderInterface $order);

  /**
   * Converts an amount in "minor unit" to a decimal amount.
   *
   * For example, 999 USD becomes 9.99.
   *
   * @param mixed $amount
   *   The amount in minor unit.
   * @param string $currency_code
   *   The currency code.
   *
   * @return \Drupal\commerce_price\Price
   *   The decimal price.
   */
  public function fromMinorUnits($amount, string $currency_code): Price;

}
