<?php

namespace Drupal\commerce_svea\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\Calculator;
use Drupal\commerce_price\Price;
use Drupal\commerce_svea\Event\SveaOrderEvent;
use Drupal\commerce_svea\Event\SveaCheckoutEvents;
use Drupal\commerce_svea\SveaManagerFactoryInterface;
use Drupal\commerce_svea\SveaValidationHandlerInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\profile\Entity\ProfileInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides the Svea checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "svea_checkout",
 *   label = "Svea Checkout",
 *   display_label = "Svea Checkout",
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_svea\PluginForm\OffsiteRedirect\SveaCheckoutForm",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class SveaCheckout extends OffsitePaymentGatewayBase implements SveaCheckoutInterface {

  /**
   * The Svea manager factory.
   *
   * @var \Drupal\commerce_svea\SveaManagerFactoryInterface
   */
  protected $sveaManagerFactory;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The Svea validation handler.
   *
   * @var \Drupal\commerce_svea\SveaValidationHandlerInterface
   */
  protected $sveaValidationHandler;

  /**
   * Constructs a new SveaCheckout object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\commerce_svea\SveaManagerFactoryInterface $svea_manager_factory
   *   The Svea manager factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\commerce_svea\SveaValidationHandlerInterface $svea_validation_handler
   *   The Svea validation handler.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    SveaManagerFactoryInterface $svea_manager_factory,
    LoggerInterface $logger,
    ModuleHandlerInterface $module_handler,
    SveaValidationHandlerInterface $svea_validation_handler,
    EventDispatcherInterface $event_dispatcher
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    $this->sveaManagerFactory = $svea_manager_factory;
    $this->logger = $logger;
    $this->moduleHandler = $module_handler;
    $this->sveaValidationHandler = $svea_validation_handler;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_svea.manager_factory'),
      $container->get('logger.channel.commerce_svea'),
      $container->get('module_handler'),
      $container->get('commerce_svea.validation_handler'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'merchant_id' => '',
      'merchant_secret' => '',
      'purchase_country' => '',
      'locale' => 'sv-SE',
      'terms_path' => '',
      'enable_order_validation' => TRUE,
      'update_billing_profile' => TRUE,
      'update_shipping_profile' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE,
    ];
    $form['merchant_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant Secret'),
      '#default_value' => $this->configuration['merchant_secret'],
      '#required' => TRUE,
    ];
    $form['purchase_country'] = [
      '#type' => 'select',
      '#options' => [
        'SE' => $this->t('Sweden'),
        'NO' => $this->t('Norway'),
        'FI' => $this->t('Finland'),
        'DK' => $this->t('Denmark'),
        'DE' => $this->t('Germany'),
      ],
      '#title' => $this->t('Purchase country'),
      '#default_value' => $this->configuration['purchase_country'],
      '#required' => TRUE,
    ];
    $form['locale'] = [
      '#type' => 'select',
      '#title' => $this->t('Locale'),
      '#default_value' => $this->configuration['locale'],
      '#required' => TRUE,
      '#options' => [
        'sv-SE' => $this->t('Swedish (sv-SE)'),
        'nn-NO' => $this->t('Norwegian (nn-NO)'),
        'nb-NO' => $this->t('Norwegian (nb-NO)'),
        'fi-FI' => $this->t('Finnish (fi-FI)'),
        'da-DK' => $this->t('Danish (da-DK)'),
        'de-DE' => $this->t('German (de-DE)'),
        'en-US' => $this->t('English (en-US)'),
      ],
    ];
    $form['terms_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Terms and conditions page path'),
      '#default_value' => $this->configuration['terms_path'],
      '#required' => TRUE,
    ];
    $form['enable_order_validation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Validate the order before it is completed'),
      '#default_value' => $this->configuration['enable_order_validation'],
    ];
    $form['update_billing_profile'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Update the billing customer profile with address information the customer enters at Svea.'),
      '#default_value' => $this->configuration['update_billing_profile'],
    ];
    $form['update_shipping_profile'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Update the shipping customer profile with address information the customer enters at Svea.'),
      '#default_value' => $this->configuration['update_shipping_profile'],
      '#access' => $this->moduleHandler->moduleExists('commerce_shipping'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchant_id'] = $values['merchant_id'];
      $this->configuration['merchant_secret'] = $values['merchant_secret'];
      $this->configuration['purchase_country'] = $values['purchase_country'];
      $this->configuration['locale'] = $values['locale'];
      $this->configuration['terms_path'] = $values['terms_path'];
      $this->configuration['enable_order_validation'] = $values['enable_order_validation'];
      $this->configuration['update_billing_profile'] = $values['update_billing_profile'];
      $this->configuration['update_shipping_profile'] = $values['update_shipping_profile'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getNotifyUrl(): Url {
    $notify_url = parent::getNotifyUrl();
    $notify_url->setOption('query', [
      'svea_order_id' => '{checkout.order.uri}',
    ]);
    return $notify_url;
  }

  /**
   * {@inheritdoc}
   */
  public function getValidationUrl(): Url {
    $validation_url = self::getNotifyUrl();
    $validation_url->mergeOptions(['query' => ['callback' => 'validation']]);
    return $validation_url;
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    switch ($request->query->get('callback')) {
      case 'validation':
        return $this->sveaValidationHandler->validateOrder($request);

      default:
        try {
          $svea_order_id = $request->query->get('svea_order_id');
          if (!$svea_order_id) {
            throw new PaymentGatewayException('Cannot acknowledge the Svea order: no order ID provided by Svea.');
          }

          $this->acknowledgeOrder($svea_order_id);
        }
        catch (\Exception $exception) {
          $message = $exception->getMessage();
          $this->logger->error('An error occurred with Svea Checkout on IPN: @message', ['@message' => $message]);
          // Svea recognises only 200-299 code as OK, and 500 and above codes as
          // error, and will try to push again in that case.
          return new JsonResponse($message, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $svea_order_id = $order->getData('svea_order_id');
    if (!$svea_order_id) {
      throw new PaymentGatewayException(sprintf('Cannot acknowledge the Svea order: unknown Svea ID for order ID %s.', $order->id()));
    }

    $this->acknowledgeOrder($svea_order_id, $order);
  }

  /**
   * Acknowledges commerce order based on the given Svea order data.
   *
   * @param string $svea_order_id
   *   The Svea order id.
   * @param \Drupal\commerce_order\Entity\OrderInterface|null $order
   *   The commerce order to acknowledge.
   */
  protected function acknowledgeOrder(string $svea_order_id, ?OrderInterface $order = NULL) {
    /** @var \Drupal\commerce_payment\PaymentStorageInterface $payment_storage */
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->loadByRemoteId($svea_order_id);

    // If a payment already exists in the system for the given order, it means
    // the order was already processed.
    if ($payment) {
      return;
    }
    try {
      $svea_manager = $this->sveaManagerFactory->get($this->getConfiguration());
      $svea_order = $svea_manager->getOrder($svea_order_id);

      // Do not proceed with the order until it's completed.
      if ($svea_order['Status'] != 'Final') {
        throw new PaymentGatewayException("Svea order can not be acknowledged, because it's not completed yet.");
      }

      $svea_client_order_id = $svea_order['ClientOrderNumber'] ?? NULL;
      if (!$svea_client_order_id) {
        throw new PaymentGatewayException('Cannot find a client order number in the Svea order.');
      }

      // If the no order was given (e.g. on "onNotify" execution).
      if (!$order instanceof OrderInterface) {
        $order_storage = $this->entityTypeManager->getStorage('commerce_order');
        $order = $order_storage->load($svea_client_order_id);
        if (!$order instanceof OrderInterface) {
          throw new PaymentGatewayException(sprintf('Cannot find a matching order for the given Svea order ID %s.', $svea_order_id));
        }
      }
      else {
        // Check whether we are operating with the same order as was given
        // from site side and the one that was received from Svea.
        if ($order->id() != $svea_client_order_id) {
          throw new PaymentGatewayException(sprintf('Order id mismatch detected: passed from site is #%s, while received from Svea is #%s.', $order->id(), $svea_client_order_id));
        }
      }

      if (isset($svea_order['EmailAddress'])) {
        $order->setEmail($svea_order['EmailAddress']);
      }

      // Update the billing profile if configured to do so.
      $configuration = $this->getConfiguration();
      if (!empty($configuration['update_billing_profile']) && isset($svea_order['BillingAddress'])) {
        $profile = $order->getBillingProfile();
        if (!$profile instanceof ProfileInterface) {
          /** @var \Drupal\profile\Entity\ProfileInterface $profile */
          $profile = $this->entityTypeManager->getStorage('profile')->create([
            'uid' => 0,
            'type' => 'customer',
          ]);
          $order->setBillingProfile($profile);
        }
        $this->populateProfile($profile, $svea_order['BillingAddress']);
        $profile->save();
      }

      // Update the shipping profile if configured to do so.
      if (!empty($configuration['update_shipping_profile']) && isset($svea_order['ShippingAddress'])) {
        $profiles = $order->collectProfiles();

        if (isset($profiles['shipping'])) {
          $this->populateProfile($profiles['shipping'], $svea_order['ShippingAddress']);
          $profiles['shipping']->save();
        }
      }

      $event = new SveaOrderEvent($order, $svea_order);
      $this->eventDispatcher->dispatch(SveaCheckoutEvents::ACKNOWLEDGE_ORDER, $event);

      // We have to save the order since the billing profile and/or the email
      // were potentially updated.
      $order->setRefreshState(OrderInterface::REFRESH_SKIP);
      $order->unlock();
      $order->save();
    }
    catch (\Exception $exception) {
      throw new PaymentGatewayException($exception->getMessage());
    }

    // Create payment.
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $payment_storage->create([
      'state' => 'authorization',
      // Right for now, save order total as payment amount, because we don't
      // have any information from Svea for that value.
      'amount' => $order->getTotalPrice(),
      'payment_gateway' => isset($this->parentEntity) ? $this->parentEntity->id() : $this->entityId,
      'order_id' => $svea_client_order_id,
      'test' => $this->getMode() === 'test',
      'remote_id' => $svea_order['OrderId'],
      'remote_state' => $svea_order['Status'],
    ]);
    $payment->save();
  }

  /**
   * Populates the given profile with the given Svea address.
   *
   * @param \Drupal\profile\Entity\ProfileInterface $profile
   *   The profile to populate.
   * @param array $address
   *   The Svea address.
   */
  protected function populateProfile(ProfileInterface $profile, array $address) {
    $mapping = [
      'FullName' => 'organization',
      'FirstName' => 'given_name',
      'LastName' => 'family_name',
      'CountryCode' => 'country_code',
      'PostalCode' => 'postal_code',
      'City' => 'locality',
      'StreetAddress' => 'address_line1',
      'StreetAddress2' => 'address_line2',
    ];

    // For B2B customers "FullName" field contains the company name, in other
    // cases it will just create duplicated data for first/last names in
    // billing/shipping profiles. Omit such cases.
    if ($address['FirstName'] || $address['LastName']) {
      // There might be cases when Svea saves name parts vice versa for some
      // reasons. So check both of possible combinations.
      $name_parts = [$address['FirstName'], $address['LastName']];
      $separator = '';
      if (in_array($this->removeSpaces($address['FullName']), [
        $this->removeSpaces(implode($separator, $name_parts)),
        $this->removeSpaces(implode($separator, array_reverse($name_parts))),
      ])) {
        unset($mapping['FullName']);
      }
    }

    foreach ($address as $key => $value) {
      if (isset($mapping[$key])) {
        $profile->address->{$mapping[$key]} = $value;
      }
    }
  }

  /**
   * Removes spaces from the string.
   *
   * @param string $string
   *   String.
   *
   * @return string
   *   String without spaces.
   */
  protected function removeSpaces(string $string): string {
    return preg_replace('/\s+/', '', $string);
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);

    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $payment->setState('completed');
    $payment->setAmount($amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);

    try {
      $svea_manager = $this->sveaManagerFactory->get($this->getConfiguration());
      $svea_manager->cancelOrder($payment->getOrder());
      $payment->setState('authorization_voided');
      $payment->save();
    }
    catch (\Exception $exception) {
      throw new PaymentGatewayException($exception->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function cancelOrder(OrderInterface $order) {
    try {
      $svea_manager = $this->sveaManagerFactory->get($this->getConfiguration());
      $svea_manager->cancelOrder($order);

      /** @var \Drupal\commerce_payment\PaymentStorageInterface $payment_storage */
      $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

      // Void all the referenced payments after cancellation.
      $payments = $payment_storage->loadMultipleByOrder($order);
      foreach ($payments as $payment) {
        $payment->getState()->applyTransitionById('void');
        $payment->save();
      }
    }
    catch (\Exception $exception) {
      $this->logger->error($exception->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function fromMinorUnits($amount, string $currency_code): Price {
    $currency_storage = $this->entityTypeManager->getStorage('commerce_currency');
    /** @var \Drupal\commerce_price\Entity\CurrencyInterface $currency */
    $currency = $currency_storage->load($currency_code);
    $fraction_digits = $currency->getFractionDigits();

    if ($fraction_digits > 0) {
      $amount = Calculator::divide((string) $amount, pow(10, $fraction_digits), $fraction_digits);
    }

    return new Price((string) $amount, $currency_code);
  }

}
